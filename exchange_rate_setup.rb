Bundler.setup
require 'exchange_rate'

ExchangeRate.config do |c|
  c[:ecb][:base_path] = File.join(__dir__, 'data')
  c[:ecb][:reference_currency_code] = 'EUR'
end
