require 'rack'
require 'bundler'
require 'date'
require 'json'

require_relative 'exchange_rate_setup'

class BadRequestError < StandardError;
end

class App
  def call(env)
    req = Rack::Request.new(env)

    begin
      (date, from, to) = get_req_params(req)
      exchange_rate = ::ExchangeRate.at(date, from, to)
      respond(200, { rate: exchange_rate })
    rescue BadRequestError
      respond(400)
    rescue Exception
      respond(500)
    end
  end

  def get_req_params(req)
    raise BadRequestError if req.params['date'].nil?
    raise BadRequestError if req.params['from'].nil?
    raise BadRequestError if req.params['to'].nil?

    [Date.parse(req.params['date']), req.params['from'], req.params['to']]
  end

  def respond(status_code, hash_body = {})
    [status_code, { "Content-Type" => "text/json" }, [JSON.generate(hash_body)]]
  end
end

run App.new
