# use_exchange_rate

Example web app using `exchange_rate` gem.

Installation steps:
    
    git clone git@bitbucket.org:ishworkh/use_exchange_rate.git
    cd use_exchange_rate
    bundle

To make it work, plz download ecb 90 days or ecb single day xml files to configured `base_path`, 
in pattern
    
    base_path/2018_06_25/file.xml
    base_path/2018_06_26/file.xml
    
for dates when exchange rate is to be determined. If a file is not available for a demand date
it will return a 500 error, as exchange rate for that day is not available.


Then, from inside the project run
    
    rackup app.ru
    

Web app should be accessible in `http://localhost:9292`

For example, to get an exchange rate for 2018/06/25, 'EUR' to 'SEK',

    curl 'http://localhost:9292/?date=2018-06-25&from=EUR&to=SEK' 
